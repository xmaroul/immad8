<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/newsplus/templates/user--full.html.twig */
class __TwigTemplate_f1b66bf8d1119ef0dc3b7cc30dc87f20d0f475a00fad2f22ffba8de9c181b534 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 23];
        $filters = ["escape" => 21, "without" => 54];
        $functions = ["attach_library" => 21];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape', 'without'],
                ['attach_library']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 21
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("newsplus/user-profile"), "html", null, true);
        echo "
<article";
        // line 22
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "profile clearfix"], "method")), "html", null, true);
        echo ">
  ";
        // line 23
        if (($context["content"] ?? null)) {
            // line 24
            echo "    ";
            if ($this->getAttribute($this->getAttribute(($context["user"] ?? null), "user_picture", []), "value", [])) {
                // line 25
                echo "      <div class=\"with-user-image\">
        <div class=\"photo-wrapper\">
          ";
                // line 27
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content"] ?? null), "user_picture", [])), "html", null, true);
                echo "
          ";
                // line 28
                if ((($this->getAttribute($this->getAttribute(($context["user"] ?? null), "field_mt_facebook", []), "value", []) || $this->getAttribute($this->getAttribute(($context["user"] ?? null), "field_mt_twitter", []), "value", [])) || $this->getAttribute($this->getAttribute(($context["user"] ?? null), "field_mt_google_plus", []), "value", []))) {
                    // line 29
                    echo "            <ul class=\"user-social-bookmarks\">
              ";
                    // line 30
                    if ($this->getAttribute($this->getAttribute(($context["user"] ?? null), "field_mt_facebook", []), "value", [])) {
                        // line 31
                        echo "                <li class=\"facebook\">
                  <a target=\"_blank\" href=\"";
                        // line 32
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["user"] ?? null), "field_mt_facebook", []), "value", [])), "html", null, true);
                        echo "\">
                    <i class=\"fa fa-facebook\"><span class=\"sr-only\">facebook</span></i>
                  </a>
                </li>
              ";
                    }
                    // line 37
                    echo "              ";
                    if ($this->getAttribute($this->getAttribute(($context["user"] ?? null), "field_mt_twitter", []), "value", [])) {
                        // line 38
                        echo "                <li class=\"twitter\">
                  <a target=\"_blank\" href=\"";
                        // line 39
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["user"] ?? null), "field_mt_twitter", []), "value", [])), "html", null, true);
                        echo "\">
                    <i class=\"fa fa-twitter\"><span class=\"sr-only\">twitter</span></i>
                  </a>
                </li>
              ";
                    }
                    // line 44
                    echo "              ";
                    if ($this->getAttribute($this->getAttribute(($context["user"] ?? null), "field_mt_google_plus", []), "value", [])) {
                        // line 45
                        echo "                <li class=\"linkedin\">
                  <a target=\"_blank\" href=\"";
                        // line 46
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["user"] ?? null), "field_mt_google_plus", []), "value", [])), "html", null, true);
                        echo "\">
                    <i class=\"fa fa-linkedin\"><span class=\"sr-only\">linkedin</span></i>
                  </a>
                </li>
              ";
                    }
                    // line 51
                    echo "            </ul>
          ";
                }
                // line 53
                echo "        </div>
        ";
                // line 54
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->withoutFilter($this->sandbox->ensureToStringAllowed(($context["content"] ?? null)), "user_picture", "field_mt_facebook", "field_mt_twitter", "field_mt_google_plus"), "html", null, true);
                echo "
      </div>
    ";
            } else {
                // line 57
                echo "      ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->withoutFilter($this->sandbox->ensureToStringAllowed(($context["content"] ?? null)), "user_picture", "field_mt_facebook", "field_mt_twitter", "field_mt_google_plus"), "html", null, true);
                echo "
      ";
                // line 58
                if ((($this->getAttribute($this->getAttribute(($context["user"] ?? null), "field_mt_facebook", []), "value", []) || $this->getAttribute($this->getAttribute(($context["user"] ?? null), "field_mt_twitter", []), "value", [])) || $this->getAttribute($this->getAttribute(($context["user"] ?? null), "field_mt_google_plus", []), "value", []))) {
                    // line 59
                    echo "        <ul class=\"user-social-bookmarks\">
          ";
                    // line 60
                    if ($this->getAttribute($this->getAttribute(($context["user"] ?? null), "field_mt_facebook", []), "value", [])) {
                        // line 61
                        echo "            <li class=\"facebook\">
              <a target=\"_blank\" href=\"";
                        // line 62
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["user"] ?? null), "field_mt_facebook", []), "value", [])), "html", null, true);
                        echo "\">
                <i class=\"fa fa-facebook\"><span class=\"sr-only\">facebook</span></i>
              </a>
            </li>
          ";
                    }
                    // line 67
                    echo "          ";
                    if ($this->getAttribute($this->getAttribute(($context["user"] ?? null), "field_mt_twitter", []), "value", [])) {
                        // line 68
                        echo "            <li class=\"twitter\">
              <a target=\"_blank\" href=\"";
                        // line 69
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["user"] ?? null), "field_mt_twitter", []), "value", [])), "html", null, true);
                        echo "\">
                <i class=\"fa fa-twitter\"><span class=\"sr-only\">twitter</span></i>
              </a>
            </li>
          ";
                    }
                    // line 74
                    echo "          ";
                    if ($this->getAttribute($this->getAttribute(($context["user"] ?? null), "field_mt_google_plus", []), "value", [])) {
                        // line 75
                        echo "            <li class=\"linkedin\">
              <a target=\"_blank\" href=\"";
                        // line 76
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["user"] ?? null), "field_mt_google_plus", []), "value", [])), "html", null, true);
                        echo "\">
                <i class=\"fa fa-linkedin\"><span class=\"sr-only\">linkedin</span></i>
              </a>
            </li>
          ";
                    }
                    // line 81
                    echo "        </ul>
      ";
                }
                // line 83
                echo "    ";
            }
            // line 84
            echo "  ";
        }
        // line 85
        echo "</article>
";
    }

    public function getTemplateName()
    {
        return "themes/custom/newsplus/templates/user--full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  196 => 85,  193 => 84,  190 => 83,  186 => 81,  178 => 76,  175 => 75,  172 => 74,  164 => 69,  161 => 68,  158 => 67,  150 => 62,  147 => 61,  145 => 60,  142 => 59,  140 => 58,  135 => 57,  129 => 54,  126 => 53,  122 => 51,  114 => 46,  111 => 45,  108 => 44,  100 => 39,  97 => 38,  94 => 37,  86 => 32,  83 => 31,  81 => 30,  78 => 29,  76 => 28,  72 => 27,  68 => 25,  65 => 24,  63 => 23,  59 => 22,  55 => 21,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/custom/newsplus/templates/user--full.html.twig", "C:\\xampp\\htdocs\\imma\\themes\\custom\\newsplus\\templates\\user--full.html.twig");
    }
}
