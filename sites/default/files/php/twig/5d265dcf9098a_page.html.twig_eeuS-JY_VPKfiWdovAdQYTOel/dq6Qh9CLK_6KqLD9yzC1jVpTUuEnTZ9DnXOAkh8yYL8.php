<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/newsplus/templates/page.html.twig */
class __TwigTemplate_406933fbf7f723c503dca1e0ac1ec458136114eeb06bed8a96dda65cf9af57ef extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 63];
        $filters = ["escape" => 65, "t" => 145];
        $functions = ["path" => 145];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['escape', 't'],
                ['path']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 61
        echo "
<header class=\"headers-wrapper\">
  ";
        // line 63
        if (($this->getAttribute(($context["page"] ?? null), "pre_header_left", []) || $this->getAttribute(($context["page"] ?? null), "pre_header_right", []))) {
            // line 64
            echo "    ";
            // line 65
            echo "    <div id=\"pre-header\" class=\"clearfix ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["color_scheme_class"] ?? null)), "html", null, true);
            echo " ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["colored_area_class"] ?? null)), "html", null, true);
            echo "\">
      <div class=\"container\">
        ";
            // line 68
            echo "        <div id=\"pre-header-inside\" class=\"clearfix\">
          <div class=\"row\">
            ";
            // line 70
            if ($this->getAttribute(($context["page"] ?? null), "pre_header_left", [])) {
                // line 71
                echo "              <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["pre_header_left_grid_class"] ?? null)), "html", null, true);
                echo "\">
                ";
                // line 73
                echo "                <div id=\"pre-header-left\" class=\"clearfix\">
                  <div class=\"pre-header-area\">
                    ";
                // line 75
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "pre_header_left", [])), "html", null, true);
                echo "
                  </div>
                </div>
                ";
                // line 79
                echo "              </div>
            ";
            }
            // line 81
            echo "            ";
            if ($this->getAttribute(($context["page"] ?? null), "pre_header_right", [])) {
                // line 82
                echo "              <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["pre_header_right_grid_class"] ?? null)), "html", null, true);
                echo "\">
                ";
                // line 84
                echo "                <div id=\"pre-header-right\" class=\"clearfix\">
                  <div class=\"pre-header-area\">
                    ";
                // line 86
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "pre_header_right", [])), "html", null, true);
                echo "
                  </div>
                </div>
                ";
                // line 90
                echo "              </div>
            ";
            }
            // line 92
            echo "          </div>
        </div>
        ";
            // line 95
            echo "      </div>
    </div>
    ";
            // line 98
            echo "  ";
        }
        // line 99
        echo "  ";
        // line 100
        echo "  ";
        if (($this->getAttribute(($context["page"] ?? null), "header_top_left", []) || $this->getAttribute(($context["page"] ?? null), "header_top_right", []))) {
            // line 101
            echo "    <div id=\"header-top\" role=\"banner\" class=\"clearfix ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["color_scheme_class"] ?? null)), "html", null, true);
            echo " ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["colored_area_class"] ?? null)), "html", null, true);
            echo "\">
      <div class=\"container\">
        ";
            // line 104
            echo "        <div id=\"header-top-inside\" class=\"clearfix\">
          <div class=\"row\">
            ";
            // line 106
            if ($this->getAttribute(($context["page"] ?? null), "header_top_left", [])) {
                // line 107
                echo "              <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header_top_inside_grid_class"] ?? null)), "html", null, true);
                echo "\">
                ";
                // line 109
                echo "                <div id=\"header-top-left\" class=\"clearfix\">
                  <div class=\"header-top-area\">

                    ";
                // line 112
                if ($this->getAttribute(($context["page"] ?? null), "header_top_left", [])) {
                    // line 113
                    echo "                      ";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header_top_left", [])), "html", null, true);
                    echo "
                    ";
                }
                // line 115
                echo "
                  </div>
                </div>
                ";
                // line 119
                echo "              </div>
            ";
            }
            // line 121
            echo "            ";
            if ($this->getAttribute(($context["page"] ?? null), "header_top_right", [])) {
                // line 122
                echo "              <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header_top_inside_grid_class"] ?? null)), "html", null, true);
                echo "\">
                ";
                // line 124
                echo "                <div id=\"header-top-right\" class=\"clearfix\">
                  <div class=\"header-top-area\">
                    ";
                // line 126
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header_top_right", [])), "html", null, true);
                echo "
                  </div>
                </div>
                ";
                // line 130
                echo "              </div>
            ";
            }
            // line 132
            echo "          </div>
        </div>
        ";
            // line 135
            echo "      </div>
    </div>
  ";
        }
        // line 138
        echo "  ";
        // line 139
        echo "
  ";
        // line 141
        echo "  ";
        if (($this->getAttribute(($context["page"] ?? null), "navigation", []) || $this->getAttribute(($context["page"] ?? null), "header", []))) {
            // line 142
            echo "    <div id=\"header\" class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["color_scheme_class"] ?? null)), "html", null, true);
            echo " ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["colored_area_class"] ?? null)), "html", null, true);
            echo "\">
      ";
            // line 143
            if (($context["mt_site_name"] ?? null)) {
                // line 144
                echo "        <div class=\"site-name site-name__header\">
          <a href=\"";
                // line 145
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getPath("<front>"));
                echo "\" title=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Home"));
                echo "\" rel=\"home\">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["mt_site_name"] ?? null)), "html", null, true);
                echo "</a>
        </div>
      ";
            }
            // line 148
            echo "      <div class=\"container\">
        ";
            // line 150
            echo "        <div id=\"header-inside\" class=\"clearfix\">
          <div class=\"row\">
            <div class=\"header-area\">
              <div class=\"";
            // line 153
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header_inside_left_grid_class"] ?? null)), "html", null, true);
            echo "\">
                ";
            // line 155
            echo "                <div id=\"header-inside-left\" class=\"clearfix\">
                  ";
            // line 157
            echo "                  <div id=\"main-navigation\" class=\"clearfix\">
                    <nav role=\"navigation\">
                      ";
            // line 159
            if ($this->getAttribute(($context["page"] ?? null), "navigation", [])) {
                // line 160
                echo "                        ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation", [])), "html", null, true);
                echo "
                      ";
            }
            // line 162
            echo "                    </nav>
                  </div>
                  ";
            // line 165
            echo "                </div>
                ";
            // line 167
            echo "              </div>
              ";
            // line 168
            if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
                // line 169
                echo "                <div class=\"col-md-4\">
                  ";
                // line 171
                echo "                  <div id=\"header-inside-right\" class=\"clearfix\">
                    ";
                // line 172
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
                echo "
                  </div>
                  ";
                // line 175
                echo "                </div>
              ";
            }
            // line 177
            echo "            </div>
          </div>
        </div>
        ";
            // line 181
            echo "      </div>
    </div>
  ";
        }
        // line 184
        echo "  ";
        // line 185
        echo "</header>

";
        // line 187
        if ($this->getAttribute(($context["page"] ?? null), "page_intro", [])) {
            // line 188
            echo "  ";
            // line 189
            echo "  <div id=\"page-intro\" class=\"clearfix\">
    <div class=\"container\">
      <div class=\"row\">
        <div class=\"col-md-12\">
          ";
            // line 194
            echo "          <div id=\"page-intro-inside\" class=\"clearfix\">
            ";
            // line 195
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "page_intro", [])), "html", null, true);
            echo "
          </div>
          ";
            // line 198
            echo "        </div>
      </div>
    </div>
  </div>
  ";
        }
        // line 204
        echo "
";
        // line 206
        echo "<div id=\"page\" class=\"clearfix\">
  <div class=\"container\">
    ";
        // line 209
        echo "    <div id=\"page-inside\">

      ";
        // line 211
        if ($this->getAttribute(($context["page"] ?? null), "top_content", [])) {
            // line 212
            echo "        ";
            // line 213
            echo "        <div id=\"top-content\">
          ";
            // line 215
            echo "          <div id=\"top-content-inside\" class=\"clearfix\">
            <div class=\"row\">
              <div class=\"col-md-12\">
                ";
            // line 218
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "top_content", [])), "html", null, true);
            echo "
              </div>
            </div>
          </div>
          ";
            // line 223
            echo "        </div>
        ";
            // line 225
            echo "      ";
        }
        // line 226
        echo "
      ";
        // line 228
        echo "      <div id=\"main-content\">
        <div class=\"row\">
          ";
        // line 230
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 231
            echo "            <aside class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebar_first_grid_class"] ?? null)), "html", null, true);
            echo "\">
              ";
            // line 233
            echo "              <section id=\"sidebar-first\" class=\"sidebar clearfix\">
                ";
            // line 234
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
            echo "
              </section>
              ";
            // line 237
            echo "            </aside>
          ";
        }
        // line 239
        echo "
          <section class=\"";
        // line 240
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["main_grid_class"] ?? null)), "html", null, true);
        echo "\">

            ";
        // line 242
        if ($this->getAttribute(($context["page"] ?? null), "banner", [])) {
            // line 243
            echo "              ";
            // line 244
            echo "              <div id=\"banner\">
                ";
            // line 246
            echo "                <div id=\"banner-inside\" class=\"clearfix\">
                  ";
            // line 247
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "banner", [])), "html", null, true);
            echo "
                </div>
                ";
            // line 250
            echo "              </div>
              ";
            // line 252
            echo "            ";
        }
        // line 253
        echo "
            ";
        // line 254
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 255
            echo "              ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
            echo "
            ";
        }
        // line 257
        echo "
            ";
        // line 258
        if ($this->getAttribute(($context["page"] ?? null), "promoted", [])) {
            // line 259
            echo "              ";
            // line 260
            echo "              <div id=\"promoted\" class=\"clearfix\">
                ";
            // line 262
            echo "                <div id=\"promoted-inside\" class=\"clearfix\">
                  ";
            // line 263
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "promoted", [])), "html", null, true);
            echo "
                </div>
                ";
            // line 266
            echo "              </div>
              ";
            // line 268
            echo "            ";
        }
        // line 269
        echo "
            ";
        // line 271
        echo "            <div id=\"main\" class=\"clearfix\">

              ";
        // line 273
        if ($this->getAttribute(($context["page"] ?? null), "content", [])) {
            // line 274
            echo "                ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
            echo "
              ";
        }
        // line 276
        echo "
            </div>
            ";
        // line 279
        echo "          </section>

          ";
        // line 281
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 282
            echo "          <aside class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["sidebar_second_grid_class"] ?? null)), "html", null, true);
            echo "\">
            ";
            // line 284
            echo "            <section id=\"sidebar-second\" class=\"sidebar clearfix\">
              ";
            // line 285
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
            echo "
            </section>
            ";
            // line 288
            echo "          </aside>
          ";
        }
        // line 290
        echo "
        </div>

      </div>
      ";
        // line 295
        echo "
    </div>
    ";
        // line 298
        echo "
  </div>

</div>
";
        // line 303
        echo "

";
        // line 305
        if (((($this->getAttribute(($context["page"] ?? null), "footer_first", []) || $this->getAttribute(($context["page"] ?? null), "footer_second", [])) || $this->getAttribute(($context["page"] ?? null), "footer_third", [])) || $this->getAttribute(($context["page"] ?? null), "footer_fourth", []))) {
            // line 306
            echo "  ";
            // line 307
            echo "  <footer id=\"footer\" class=\"clearfix\">
    <div class=\"container\">

      <div class=\"row\">
        ";
            // line 311
            if ($this->getAttribute(($context["page"] ?? null), "footer_first", [])) {
                // line 312
                echo "          <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_grid_class"] ?? null)), "html", null, true);
                echo "\">
            <div class=\"footer-area\">
              ";
                // line 314
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_first", [])), "html", null, true);
                echo "
            </div>
          </div>
        ";
            }
            // line 318
            echo "
        ";
            // line 319
            if ($this->getAttribute(($context["page"] ?? null), "footer_second", [])) {
                // line 320
                echo "          <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_grid_class"] ?? null)), "html", null, true);
                echo "\">
            <div class=\"footer-area\">
              ";
                // line 322
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_second", [])), "html", null, true);
                echo "
            </div>
          </div>
        ";
            }
            // line 326
            echo "
        ";
            // line 327
            if ($this->getAttribute(($context["page"] ?? null), "footer_third", [])) {
                // line 328
                echo "          <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_grid_class"] ?? null)), "html", null, true);
                echo "\">
            <div class=\"footer-area\">
              ";
                // line 330
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_third", [])), "html", null, true);
                echo "
            </div>
          </div>
        ";
            }
            // line 334
            echo "
        ";
            // line 335
            if ($this->getAttribute(($context["page"] ?? null), "footer_fourth", [])) {
                // line 336
                echo "          <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_grid_class"] ?? null)), "html", null, true);
                echo "\">
            <div class=\"footer-area\">
              ";
                // line 338
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_fourth", [])), "html", null, true);
                echo "
            </div>
          </div>
        ";
            }
            // line 342
            echo "      </div>

    </div>
  </footer>
  ";
        }
        // line 348
        echo "
";
        // line 349
        if (($this->getAttribute(($context["page"] ?? null), "sub_footer_left", []) || $this->getAttribute(($context["page"] ?? null), "footer", []))) {
            // line 350
            echo "  ";
            // line 351
            echo "  <div id=\"subfooter\" class=\"clearfix\">
    <div class=\"container\">
      ";
            // line 354
            echo "      <div id=\"subfooter-inside\" class=\"clearfix\">
        <div class=\"row\">
          <div class=\"col-md-4\">
            ";
            // line 358
            echo "            ";
            if ($this->getAttribute(($context["page"] ?? null), "sub_footer_left", [])) {
                // line 359
                echo "              <div class=\"subfooter-area left\">
                ";
                // line 360
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sub_footer_left", [])), "html", null, true);
                echo "
              </div>
            ";
            }
            // line 363
            echo "            ";
            // line 364
            echo "          </div>
          <div class=\"col-md-8\">
            ";
            // line 367
            echo "            ";
            if ($this->getAttribute(($context["page"] ?? null), "footer", [])) {
                // line 368
                echo "              <div class=\"subfooter-area right\">
                ";
                // line 369
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer", [])), "html", null, true);
                echo "
              </div>
            ";
            }
            // line 372
            echo "            ";
            // line 373
            echo "          </div>
        </div>
      </div>
      ";
            // line 377
            echo "    </div>
  </div>";
        }
        // line 380
        echo "
";
        // line 381
        if (($context["scroll_to_top"] ?? null)) {
            // line 382
            echo "  ";
            // line 383
            echo "  <div id=\"toTop\">
    <i class=\"fa fa-play-circle fa-rotate-270\"></i>
  </div>
  ";
        }
    }

    public function getTemplateName()
    {
        return "themes/custom/newsplus/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  669 => 383,  667 => 382,  665 => 381,  662 => 380,  658 => 377,  653 => 373,  651 => 372,  645 => 369,  642 => 368,  639 => 367,  635 => 364,  633 => 363,  627 => 360,  624 => 359,  621 => 358,  616 => 354,  612 => 351,  610 => 350,  608 => 349,  605 => 348,  598 => 342,  591 => 338,  585 => 336,  583 => 335,  580 => 334,  573 => 330,  567 => 328,  565 => 327,  562 => 326,  555 => 322,  549 => 320,  547 => 319,  544 => 318,  537 => 314,  531 => 312,  529 => 311,  523 => 307,  521 => 306,  519 => 305,  515 => 303,  509 => 298,  505 => 295,  499 => 290,  495 => 288,  490 => 285,  487 => 284,  482 => 282,  480 => 281,  476 => 279,  472 => 276,  466 => 274,  464 => 273,  460 => 271,  457 => 269,  454 => 268,  451 => 266,  446 => 263,  443 => 262,  440 => 260,  438 => 259,  436 => 258,  433 => 257,  427 => 255,  425 => 254,  422 => 253,  419 => 252,  416 => 250,  411 => 247,  408 => 246,  405 => 244,  403 => 243,  401 => 242,  396 => 240,  393 => 239,  389 => 237,  384 => 234,  381 => 233,  376 => 231,  374 => 230,  370 => 228,  367 => 226,  364 => 225,  361 => 223,  354 => 218,  349 => 215,  346 => 213,  344 => 212,  342 => 211,  338 => 209,  334 => 206,  331 => 204,  324 => 198,  319 => 195,  316 => 194,  310 => 189,  308 => 188,  306 => 187,  302 => 185,  300 => 184,  295 => 181,  290 => 177,  286 => 175,  281 => 172,  278 => 171,  275 => 169,  273 => 168,  270 => 167,  267 => 165,  263 => 162,  257 => 160,  255 => 159,  251 => 157,  248 => 155,  244 => 153,  239 => 150,  236 => 148,  226 => 145,  223 => 144,  221 => 143,  214 => 142,  211 => 141,  208 => 139,  206 => 138,  201 => 135,  197 => 132,  193 => 130,  187 => 126,  183 => 124,  178 => 122,  175 => 121,  171 => 119,  166 => 115,  160 => 113,  158 => 112,  153 => 109,  148 => 107,  146 => 106,  142 => 104,  134 => 101,  131 => 100,  129 => 99,  126 => 98,  122 => 95,  118 => 92,  114 => 90,  108 => 86,  104 => 84,  99 => 82,  96 => 81,  92 => 79,  86 => 75,  82 => 73,  77 => 71,  75 => 70,  71 => 68,  63 => 65,  61 => 64,  59 => 63,  55 => 61,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/custom/newsplus/templates/page.html.twig", "C:\\xampp\\htdocs\\imma\\themes\\custom\\newsplus\\templates\\page.html.twig");
    }
}
